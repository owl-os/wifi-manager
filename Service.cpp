#include <stdio.h>
#include <memory>
#include <gio/gio.h>
#include <uuid/uuid.h>

#include <nm-dbus-interface.h>

#include <NetworkManager.h>

char *
nm_utils_uuid_generate(void)
{
    uuid_t uuid;
    char *buf;

    buf = (char*)g_malloc0(37);
    uuid_generate_random(uuid);
    uuid_unparse_lower(uuid, &buf[0]);
    return buf;
}

static void
add_connection(GDBusProxy *proxy, const char *con_name, const char *ssid, const char *psk)
{
    GVariantBuilder connection_builder;
    GVariantBuilder setting_builder;
    char           *uuid;
    const char     *new_con_path;
    GVariant       *ret;
    GError         *error = NULL;

    /* Initialize connection GVariantBuilder */
    g_variant_builder_init(&connection_builder, G_VARIANT_TYPE("a{sa{sv}}"));

    /* Build up the 'connection' Setting */
    g_variant_builder_init(&setting_builder, G_VARIANT_TYPE("a{sv}"));

    uuid = nm_utils_uuid_generate();
    g_variant_builder_add(&setting_builder,
                          "{sv}",
                          NM_SETTING_CONNECTION_UUID,
                          g_variant_new_string(uuid));
    g_free(uuid);

    g_variant_builder_add(&setting_builder,
                          "{sv}",
                          NM_SETTING_CONNECTION_ID,
                          g_variant_new_string(con_name));
    g_variant_builder_add(&setting_builder,
                          "{sv}",
                          NM_SETTING_CONNECTION_TYPE,
                          g_variant_new_string(NM_SETTING_WIRELESS_SETTING_NAME));               
    g_variant_builder_add(&connection_builder,
                          "{sa{sv}}",
                          NM_SETTING_CONNECTION_SETTING_NAME,
                          &setting_builder);

    /* Add the (empty) 'wired' Setting */
    g_variant_builder_init(&setting_builder, G_VARIANT_TYPE("a{sv}"));
    g_variant_builder_add(&connection_builder,
                          "{sa{sv}}",
                          NM_SETTING_WIRED_SETTING_NAME,
                          &setting_builder);

    /* Add wireless Setting */
    g_variant_builder_init(&setting_builder, G_VARIANT_TYPE("a{sv}"));
    g_variant_builder_add(&setting_builder,
                          "{sv}",
                          NM_SETTING_WIRELESS_SSID,
                          g_variant_new_bytestring(ssid));
    g_variant_builder_add(&setting_builder,
                          "{sv}",
                          NM_SETTING_WIRELESS_MODE,
                          g_variant_new_string(NM_SETTING_WIRELESS_MODE_INFRA));
    g_variant_builder_add(&connection_builder,
                          "{sa{sv}}",
                          NM_SETTING_WIRELESS_SETTING_NAME,
                          &setting_builder);

    /* Add wireless security Setting */
    g_variant_builder_init(&setting_builder, G_VARIANT_TYPE("a{sv}"));                      
    g_variant_builder_add(&setting_builder,
                          "{sv}",
                          NM_SETTING_WIRELESS_SECURITY_KEY_MGMT,
                          g_variant_new_string("wpa-psk"));
    g_variant_builder_add(&setting_builder,
                          "{sv}",
                          NM_SETTING_WIRELESS_SECURITY_AUTH_ALG,
                          g_variant_new_string("open"));
    g_variant_builder_add(&setting_builder,
                          "{sv}",
                          NM_SETTING_WIRELESS_SECURITY_PSK,
                          g_variant_new_string(psk));
    g_variant_builder_add(&connection_builder,
                          "{sa{sv}}",
                          NM_SETTING_WIRELESS_SECURITY_SETTING_NAME,
                          &setting_builder);

    /* Build up the 'ipv4' Setting */
    g_variant_builder_init(&setting_builder, G_VARIANT_TYPE("a{sv}"));
    g_variant_builder_add(&setting_builder,
                          "{sv}",
                          NM_SETTING_IP_CONFIG_METHOD,
                          g_variant_new_string(NM_SETTING_IP4_CONFIG_METHOD_AUTO));
    g_variant_builder_add(&connection_builder,
                          "{sa{sv}}",
                          NM_SETTING_IP4_CONFIG_SETTING_NAME,
                          &setting_builder);

    /* Build up the 'ipv6' Setting */
    g_variant_builder_init(&setting_builder, G_VARIANT_TYPE("a{sv}"));
    g_variant_builder_add(&setting_builder,
                          "{sv}",
                          NM_SETTING_IP_CONFIG_METHOD,
                          g_variant_new_string(NM_SETTING_IP6_CONFIG_METHOD_IGNORE));
    g_variant_builder_add(&connection_builder,
                          "{sa{sv}}",
                          NM_SETTING_IP6_CONFIG_SETTING_NAME,
                          &setting_builder);

    ret = g_dbus_proxy_call_sync(proxy,
                                 "AddConnection",
                                 g_variant_new("(a{sa{sv}})", &connection_builder),
                                 G_DBUS_CALL_FLAGS_NONE,
                                 -1,
                                 NULL,
                                 &error);
    if (ret) {
        g_variant_get(ret, "(&o)", &new_con_path);
        g_print("Added: %s\n", new_con_path);
        g_variant_unref(ret);
    } else {
        g_dbus_error_strip_remote_error(error);
        g_print("Error adding connection: %s\n", error->message);
        g_clear_error(&error);
    }     
}

int 
main(int argc, char *argv[])
{
    GDBusProxy *proxy;
    GError *error = NULL;

    proxy = g_dbus_proxy_new_for_bus_sync(G_BUS_TYPE_SYSTEM,
                                          G_DBUS_PROXY_FLAGS_NONE,
                                          NULL,
                                          NM_DBUS_SERVICE,
                                          NM_DBUS_PATH_SETTINGS,
                                          NM_DBUS_INTERFACE_SETTINGS,
                                          NULL,
                                          &error);

    if (!proxy) {
        g_dbus_error_strip_remote_error(error);
        g_print("Could not create NetworkManager D-Bus proxy: %s\n", error->message);
        g_error_free(error);
        return 1;
    }

    add_connection(proxy, "Vinaphone", "Vinaphone", "1234512345");

    g_object_unref(proxy);

    return 0;
}