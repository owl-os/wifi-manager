/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/libcore/luni/src/main/java/java/io/ByteArrayOutputStream.java
 */
/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef DIAG_BYTE_ARRAY_OUTPUT_STREAM_HPP_
#define DIAG_BYTE_ARRAY_OUTPUT_STREAM_HPP_

#include <mutex>

namespace android
{

    class ByteArrayOutputStream
    {
    public:
        explicit ByteArrayOutputStream(int32_t size);
        virtual ~ByteArrayOutputStream();

        uint8_t *toByteArray();
        void write(int32_t oneByte);
        void write(uint8_t *buffer, int32_t offset, int32_t length);
        int32_t getCount();

    protected:
        /**
         * The byte array containing the bytes written.
         */
        uint8_t *mBuf;

        /**
         * The corrent position within the byte array.
         */
        int32_t mPos;

        /**
         * The number of bytes written.
         */
        int32_t mCount;

    private:
        void expand(int32_t i);

        std::mutex mMutex;
    };

} // namespace android

#endif // DIAG_BYTE_ARRAY_OUTPUT_STREAM_HPP_
