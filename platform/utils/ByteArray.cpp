/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/dalvik/dx/src/com/android/dx/util/ByteArray.java
 */
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ByteArray.hpp>

#include <algorithm>
#include <cassert>
#include <stdexcept>

namespace android
{

    namespace
    {

        constexpr const char *const kHexChars = "0123456789abcdef";

    } // namespace

    ByteArray::ByteArray(const uint8_t *bytes, uint64_t size)
        : mBytes(std::basic_string<uint8_t>(bytes, size))
    {
    }

    ByteArray::ByteArray(uint64_t size)
        : mBytes(std::basic_string<uint8_t>(size, 0x00U))
    {
    }

    ByteArray::ByteArray(const ByteArray &byteArray)
        : mBytes(byteArray.mBytes)
    {
    }

    ByteArray::ByteArray(const std::basic_string<uint8_t> &bytes)
        : mBytes(bytes)
    {
    }

    ByteArray &ByteArray::operator=(const ByteArray &other)
    {
        mBytes = other.mBytes;
        return *this;
    }

    const uint8_t &ByteArray::operator[](int32_t offset) const
    {
        checkOffsets(offset, offset + 1);
        return mBytes[offset];
    }

    uint8_t &ByteArray::operator[](int32_t offset)
    {
        checkOffsets(offset, offset + 1);
        return mBytes[offset];
    }

    bool ByteArray::empty() const
    {
        return mBytes.size() == 0;
    }

    uint64_t ByteArray::size() const
    {
        return mBytes.size();
    }

    void ByteArray::putByte(int32_t offset, uint8_t value)
    {
        checkOffsets(offset, offset + 1);
        mBytes[offset] = value;
    }

    uint32_t ByteArray::getByte(int32_t offset) const
    {
        return getUint8(offset);
    }

    int32_t ByteArray::getInt8(int32_t offset) const
    {
        checkOffsets(offset, offset + 1);
        return static_cast<int8_t>(getByte0(offset));
    }

    int32_t ByteArray::getInt16(int32_t offset) const
    {
        checkOffsets(offset, offset + 2);
        return static_cast<int16_t>((getByte0(offset + 1) << 8U) | getByte0(offset));
    }

    int32_t ByteArray::getInt32(int32_t offset) const
    {
        checkOffsets(offset, offset + 4);
        return static_cast<int32_t>(
            (getByte0(offset + 3) << 24U) |
            (getByte0(offset + 2) << 16U) |
            (getByte0(offset + 1) << 8U) |
            getByte0(offset));
    }

    int64_t ByteArray::getInt64(int32_t offset) const
    {
        checkOffsets(offset, offset + 8);
        uint64_t part1 = (getByte0(offset + 3) << 24U) |
                         (getByte0(offset + 2) << 16U) |
                         (getByte0(offset + 1) << 8U) |
                         getByte0(offset);
        uint64_t part2 = (getByte0(offset + 7) << 24U) |
                         (getByte0(offset + 6) << 16U) |
                         (getByte0(offset + 5) << 8U) |
                         getByte0(offset + 4);
        return static_cast<int64_t>((part1 & 0xffffffffULL) | (part2 << 32U));
    }

    uint32_t ByteArray::getUint8(int32_t offset) const
    {
        checkOffsets(offset, offset + 1);
        return getByte0(offset);
    }

    uint32_t ByteArray::getUint16(int32_t offset) const
    {
        checkOffsets(offset, offset + 2);
        return (getByte0(offset + 1) << 8U) | getByte0(offset);
    }

    uint32_t ByteArray::getUint32(int32_t offset) const
    {
        checkOffsets(offset, offset + 4);
        return (getByte0(offset + 3) << 24U) |
               (getByte0(offset + 2) << 16U) |
               (getByte0(offset + 1) << 8U) |
               getByte0(offset);
    }

    uint64_t ByteArray::getUint64(int32_t offset) const
    {
        checkOffsets(offset, offset + 8);
        uint64_t part1 = (getByte0(offset + 3) << 24U) |
                         (getByte0(offset + 2) << 16U) |
                         (getByte0(offset + 1) << 8U) |
                         getByte0(offset);
        uint64_t part2 = (getByte0(offset + 7) << 24U) |
                         (getByte0(offset + 6) << 16U) |
                         (getByte0(offset + 5) << 8U) |
                         getByte0(offset + 4);
        return (part1 & 0xffffffffULL) | (part2 << 32U);
    }

    const uint8_t *ByteArray::getBytes(int32_t offset) const
    {
        checkOffsets(offset, mBytes.size());
        return mBytes.data() + offset;
    }

    std::basic_string<uint8_t> ByteArray::getByteArray() const
    {
        return mBytes;
    }

    std::vector<uint8_t> ByteArray::getVector() const
    {
        std::vector<uint8_t> result;
        std::for_each(std::begin(mBytes), std::end(mBytes), [&result](const uint8_t &byte) {
            result.push_back(byte);
        });
        return result;
    }

    std::string ByteArray::getString(int32_t offset) const
    {
        if (mBytes.size() > static_cast<uint32_t>(offset))
        {
            return getString(offset, mBytes.size() - offset);
        }
        else
        {
            return "";
        }
    }

    std::string ByteArray::getString(int32_t offset, uint64_t size) const
    {
        checkOffsets(offset, offset + size);
        std::string s;
        for (uint32_t i = offset; i < offset + size; ++i)
        {
            s += static_cast<char>(mBytes[i]);
        }
        return s;
    }

    std::u16string ByteArray::getU16String(int32_t offset) const
    {
        return getU16String(offset, mBytes.size() - offset);
    }

    std::u16string ByteArray::getU16String(int32_t offset, uint64_t size) const
    {
        checkOffsets(offset, offset + size);
        std::u16string s16;
        for (uint32_t i = offset; i < size; i += 2)
        {
            s16.append(1, static_cast<char16_t>((getByte0(i) << 8U) | getByte0(i + 1)));
        }
        return s16;
    }

    std::string ByteArray::toString() const
    {
        std::string s;

        uint32_t idx;
        for (auto const &byte : mBytes)
        {
            idx = 0x0fU & (byte >> 4U);
            s += kHexChars[idx];
            idx = 0x0fU & byte;
            s += kHexChars[idx];
        }
        return s;
    }

    void ByteArray::checkOffsets(int32_t start, int32_t end) const
    {
        if ((end <= start) || (end > static_cast<int32_t>(mBytes.size())))
        {
            std::string what;
            what += "bad range: ";
            what += std::to_string(start);
            what += "..";
            what += std::to_string(end);
            what += "; actual size";
            what += std::to_string(mBytes.size());
            // throw std::out_of_range(what);
        }
    }

    uint32_t ByteArray::getByte0(int32_t offset) const
    {
        return mBytes[offset];
    }

} // namespace android
