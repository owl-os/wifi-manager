/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/dalvik/dx/src/com/android/dx/util/ByteArray.java
 */
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <Buffer.hpp>

namespace android
{
    Buffer::Buffer()
    {

    }
    Buffer::Buffer(Buffer& other)
    {
        setTo(other);
    }
    Buffer::~Buffer()
    {

    }
    Buffer& Buffer::operator=(const Buffer& other)
    {
        setTo(other);
        return *this;
    }
    void Buffer::setTo(const Buffer& set_buf)
    {
        if (this != &set_buf)
        {
            mVec.clear();
            mVec.shrink_to_fit();
            mVec.reserve(set_buf.size());
            mVec = set_buf.mVec;
        }
    }
    void Buffer::setTo(uint8_t* buf, int32_t len)
    {
        mVec.clear();
        mVec.shrink_to_fit();
        mVec.reserve(len);
        mVec.insert(mVec.end(), buf, buf + len);
    }
    void Buffer::setTo(char* buf, int32_t len)
    {
        mVec.clear();
        mVec.shrink_to_fit();
        mVec.reserve(len);
        mVec.insert(mVec.end(), buf, buf + len);
    }
    void Buffer::setTo(uint8_t* buf, int32_t offset, int32_t len)
    {
        mVec.clear();
        mVec.shrink_to_fit();

        if(offset >= len){
            //error
        }
        else{
            mVec.reserve(len - offset);
            mVec.insert(mVec.end(), buf + offset, buf + len);
        }
    }
    void Buffer::setTo(char* buf, int32_t offset, int32_t len)
    {
        mVec.clear();
        mVec.shrink_to_fit();

        if(offset >= len){
            //error
        }
        else{
            mVec.reserve(len - offset);
            mVec.insert(mVec.end(), buf + offset, buf + len);
        }
    }
    void Buffer::setTo(uint64_t data)
    {
        mVec.clear();
        mVec.shrink_to_fit();

        /* big endian */
        /* 0x123456 -> mVec[0] = 0x12, mVec[1] = 0x34, mVec[2] = 0x56 */
        if(data <= 0xFF){
            mVec.push_back( data & 0x00000000000000FF);
        }
        else if(data <= 0xFFFF){
            mVec.push_back((data & 0x000000000000FF00) >> 8);
            mVec.push_back( data & 0x00000000000000FF);
        }
        else if(data <= 0xFFFFFF){
            mVec.push_back((data & 0x0000000000FF0000) >> 16);
            mVec.push_back((data & 0x000000000000FF00) >> 8);
            mVec.push_back( data & 0x00000000000000FF);
        }
        else if(data <= 0xFFFFFFFF){
            mVec.push_back((data & 0x00000000FF000000) >> 24);
            mVec.push_back((data & 0x0000000000FF0000) >> 16);
            mVec.push_back((data & 0x000000000000FF00) >> 8);
            mVec.push_back( data & 0x00000000000000FF);
        }else if(data <= 0xFFFFFFFFFF){
            mVec.push_back((data & 0x000000FF00000000) >> 32);
            mVec.push_back((data & 0x00000000FF000000) >> 24);
            mVec.push_back((data & 0x0000000000FF0000) >> 16);
            mVec.push_back((data & 0x000000000000FF00) >> 8);
            mVec.push_back( data & 0x00000000000000FF);
        }else if(data <= 0xFFFFFFFFFFFF){
            mVec.push_back((data & 0x0000FF0000000000) >> 40);
            mVec.push_back((data & 0x000000FF00000000) >> 32);
            mVec.push_back((data & 0x00000000FF000000) >> 24);
            mVec.push_back((data & 0x0000000000FF0000) >> 16);
            mVec.push_back((data & 0x000000000000FF00) >> 8);
            mVec.push_back( data & 0x00000000000000FF);
        }else if(data <= 0xFFFFFFFFFFFFFF){
            mVec.push_back((data & 0x00FF000000000000) >> 48);
            mVec.push_back((data & 0x0000FF0000000000) >> 40);
            mVec.push_back((data & 0x000000FF00000000) >> 32);
            mVec.push_back((data & 0x00000000FF000000) >> 24);
            mVec.push_back((data & 0x0000000000FF0000) >> 16);
            mVec.push_back((data & 0x000000000000FF00) >> 8);
            mVec.push_back( data & 0x00000000000000FF);
        }else if(data <= 0xFFFFFFFFFFFFFFFF){
            mVec.push_back((data & 0xFF00000000000000) >> 56);
            mVec.push_back((data & 0x00FF000000000000) >> 48);
            mVec.push_back((data & 0x0000FF0000000000) >> 40);
            mVec.push_back((data & 0x000000FF00000000) >> 32);
            mVec.push_back((data & 0x00000000FF000000) >> 24);
            mVec.push_back((data & 0x0000000000FF0000) >> 16);
            mVec.push_back((data & 0x000000000000FF00) >> 8);
            mVec.push_back( data & 0x00000000000000FF);
        }else{
            //do nothing
        }
    }
    void Buffer::assignTo(uint32_t len, uint32_t val)
    {
        mVec.clear();
        mVec.shrink_to_fit();
        mVec.assign(len, val);
    }
    void Buffer::append(uint8_t* buf, int32_t len)
    {
        if(mVec.capacity() < (mVec.size()+len))
            mVec.reserve(mVec.size()+len);

        mVec.insert(mVec.end(), buf, buf + len);
    }
    void Buffer::append(uint64_t data)
    {
        /* big endian */
        /* 0x123456 -> mVec[0] = 0x12, mVec[1] = 0x34, mVec[2] = 0x56 */
        if(data <= 0xFF){
            mVec.push_back( data & 0x00000000000000FF);
        }
        else if(data <= 0xFFFF){
            mVec.push_back((data & 0x000000000000FF00) >> 8);
            mVec.push_back( data & 0x00000000000000FF);
        }
        else if(data <= 0xFFFFFF){
            mVec.push_back((data & 0x0000000000FF0000) >> 16);
            mVec.push_back((data & 0x000000000000FF00) >> 8);
            mVec.push_back( data & 0x00000000000000FF);
        }
        else if(data <= 0xFFFFFFFF){
            mVec.push_back((data & 0x00000000FF000000) >> 24);
            mVec.push_back((data & 0x0000000000FF0000) >> 16);
            mVec.push_back((data & 0x000000000000FF00) >> 8);
            mVec.push_back( data & 0x00000000000000FF);
        }else if(data <= 0xFFFFFFFFFF){
            mVec.push_back((data & 0x000000FF00000000) >> 32);
            mVec.push_back((data & 0x00000000FF000000) >> 24);
            mVec.push_back((data & 0x0000000000FF0000) >> 16);
            mVec.push_back((data & 0x000000000000FF00) >> 8);
            mVec.push_back( data & 0x00000000000000FF);
        }else if(data <= 0xFFFFFFFFFFFF){
            mVec.push_back((data & 0x0000FF0000000000) >> 40);
            mVec.push_back((data & 0x000000FF00000000) >> 32);
            mVec.push_back((data & 0x00000000FF000000) >> 24);
            mVec.push_back((data & 0x0000000000FF0000) >> 16);
            mVec.push_back((data & 0x000000000000FF00) >> 8);
            mVec.push_back( data & 0x00000000000000FF);
        }else if(data <= 0xFFFFFFFFFFFFFF){
            mVec.push_back((data & 0x00FF000000000000) >> 48);
            mVec.push_back((data & 0x0000FF0000000000) >> 40);
            mVec.push_back((data & 0x000000FF00000000) >> 32);
            mVec.push_back((data & 0x00000000FF000000) >> 24);
            mVec.push_back((data & 0x0000000000FF0000) >> 16);
            mVec.push_back((data & 0x000000000000FF00) >> 8);
            mVec.push_back( data & 0x00000000000000FF);
        }else if(data <= 0xFFFFFFFFFFFFFFFF){
            mVec.push_back((data & 0xFF00000000000000) >> 56);
            mVec.push_back((data & 0x00FF000000000000) >> 48);
            mVec.push_back((data & 0x0000FF0000000000) >> 40);
            mVec.push_back((data & 0x000000FF00000000) >> 32);
            mVec.push_back((data & 0x00000000FF000000) >> 24);
            mVec.push_back((data & 0x0000000000FF0000) >> 16);
            mVec.push_back((data & 0x000000000000FF00) >> 8);
            mVec.push_back( data & 0x00000000000000FF);
        }else{
            //do nothing
        }
    }
    void Buffer::copy(uint8_t* buf, uint32_t len, uint32_t offset)
    {
        if((offset == mVec.size()) || (offset + len > mVec.size())){
            //do nothing
        }
        else{
            std::copy(buf, buf + len, mVec.data() + offset);
        }
    }
    bool Buffer::isEqual(uint8_t* buf, uint32_t len)
    {
        bool ret = false;
        std::vector<uint8_t> temp(buf, buf + len);

        if(mVec.size() != len){
            ret = false;
        }
        else{
            ret = (mVec == temp) ? true : false;
        }

        return ret;
    }
    void Buffer::clear()
    {
        mVec.clear();
        mVec.shrink_to_fit();
    }
    uint8_t* Buffer::data()
    {
        return (uint8_t*)mVec.data();
    }
    uint32_t Buffer::size() const
    {
        return mVec.size();
    }
    bool Buffer::empty()
    {
        return mVec.empty();
    }
    std::vector<uint8_t>* Buffer::containerPointer()
    {
        return &mVec;
    }

} // namespace android
