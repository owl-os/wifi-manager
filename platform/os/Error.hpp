#ifndef ERROR_H
#define ERROR_H

typedef unsigned char   uint8_t;
typedef unsigned short  uint16_t;
typedef unsigned int    uint32_t;
typedef signed char     int8_t;
typedef signed short    int16_t;
typedef signed int      int32_t;
typedef signed int      error_t;

enum ANALYZER_ERR {
    E_OK               =  0,  /**< (  0)  No error */
    E_ERROR            = -1,  /**< ( -1)  Unknown error */
    E_INVALID_PARAM    = -2,  /**< ( -2)  Invalid parameter */
    E_CAN_NOT_RESPONSE = -3,  /**< ( -3)  Can not send response */
    E_TIME_OUT         = -4,  /**< ( -4)  Occur time-out */
    E_REJECTED         = -5,  /**< ( -5)  Request is rejected due to some reason */
    E_PENDING          = -6,  /**< ( -6)  Request is pending due to some reason  */
    E_NOT_ENOUGH_MEMORY= -7,  /**< ( -7)  Not enough memory */
    E_DATA_CORRUPTED   = -8,  /**< ( -8)  Data corrupted */
    E_BUFFER_EMPTY     = -9,  /**< ( -9)  Buffer empty */
    E_INPUT_EMPTY      = -10, /**< (-10)  Input empty */
    E_FRAME_NOTREADY   = -11  /**< (-11)  Frame Not Ready */
};

#endif