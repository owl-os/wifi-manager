/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/RegistrantList.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <RegistrantList.hpp>

#include <algorithm>

#include <Registrant.hpp>

namespace android
{

    RegistrantList::RegistrantList()
    {
    }

    RegistrantList::~RegistrantList()
    {
    }

    void RegistrantList::add(const std::shared_ptr<Handler> &h, int32_t what, const std::shared_ptr<void> &obj)
    {
        std::lock_guard<std::recursive_mutex> lock(mMutex);

        add(std::make_shared<Registrant>(h, what, obj));
    }

    void RegistrantList::addUnique(const std::shared_ptr<Handler> &h, int32_t what, const std::shared_ptr<void> &obj)
    {
        std::lock_guard<std::recursive_mutex> lock(mMutex);

        remove(h);
        add(std::make_shared<Registrant>(h, what, obj));
    }

    void RegistrantList::add(const std::shared_ptr<Registrant> &r)
    {
        std::lock_guard<std::recursive_mutex> lock(mMutex);

        removeCleared();
        mRegistrants.push_back(r);
    }

    void RegistrantList::removeCleared()
    {
        std::lock_guard<std::recursive_mutex> lock(mMutex);

        mRegistrants.erase(
            std::remove_if(mRegistrants.begin(), mRegistrants.end(),
                           [](const std::shared_ptr<Registrant> &r) {
                               return r->getHandler() == nullptr;
                           }),
            mRegistrants.end());
    }

    uint64_t RegistrantList::size()
    {
        std::lock_guard<std::recursive_mutex> lock(mMutex);

        return mRegistrants.size();
    }

    void RegistrantList::internalNotifyRegistrants(const std::shared_ptr<void> &result, const std::shared_ptr<std::exception> &ex)
    {
        for (auto const &r : mRegistrants)
        {
            r->internalNotifyRegistrant(result, ex);
        }
    }

    void RegistrantList::notifyRegistrants()
    {
        internalNotifyRegistrants(nullptr, nullptr);
    }

    void RegistrantList::notifyException(const std::shared_ptr<std::exception> &ex)
    {
        internalNotifyRegistrants(nullptr, ex);
    }

    void RegistrantList::notifyResult(const std::shared_ptr<void> &result)
    {
        internalNotifyRegistrants(result, nullptr);
    }

    void RegistrantList::notifyRegistrants(const std::shared_ptr<AsyncResult> &ar)
    {
        internalNotifyRegistrants(ar->result, ar->excption);
    }

    void RegistrantList::remove(const std::shared_ptr<Handler> &h)
    {
        std::lock_guard<std::recursive_mutex> lock(mMutex);
        mRegistrants.erase(
            std::remove_if(mRegistrants.begin(), mRegistrants.end(),
                           [h](const std::shared_ptr<Registrant> &r) {
                               return r->getHandler() == nullptr || r->getHandler() == h;
                           }),
            mRegistrants.end());
    }

} // namespace android
