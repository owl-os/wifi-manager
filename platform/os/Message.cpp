/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/Message.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <Message.hpp>

#include <cassert>
#include <sstream>

#include <Handler.hpp>
#include <Tlog.hpp>
#include <SystemClock.hpp>
#include <TimeHelper.hpp>

namespace android
{

    namespace
    {

        constexpr const char *const kLogTag = "Message";

        constexpr uint32_t kFlagInUse = 0x01;
        constexpr uint32_t kFlagAsynchronous = 0x02;
        constexpr uint32_t kFlagToClearOnCopyFrom = kFlagInUse;

        struct MessagePool
        {
            constexpr int32_t static maxSize = 50;
            int32_t size = 0;
            std::shared_ptr<Message> header;
            std::mutex mutex_;
        };

        MessagePool sPool;

    } // namespace

    Message::Message()
        : what(0),
          arg1(0),
          arg2(0),
          arg3(0),
          obj(nullptr),
          flags(0),
          when(0ULL),
          target(nullptr),
          next(nullptr)
    {
    }

    std::shared_ptr<Message> Message::obtain()
    {
        {
            std::lock_guard<std::mutex> lock(sPool.mutex_);
            if (sPool.header != nullptr)
            {
                auto m = sPool.header;
                sPool.header = m->next;
                m->next = nullptr;
                m->flags = 0; // clear in-use flag
                sPool.size--;
                return m;
            }
        }
        auto m = std::make_shared<Message>();
        return m;
    }

    std::shared_ptr<Message> Message::obtain(const std::shared_ptr<Message> &orig)
    {
        auto m = obtain();
        m->what = orig->what;
        m->arg1 = orig->arg1;
        m->arg2 = orig->arg2;
        m->obj = orig->obj;
        m->target = orig->target;
        return m;
    }

    std::shared_ptr<Message> Message::obtain(const std::shared_ptr<Handler> &h)
    {
        auto m = obtain();
        m->target = h;
        return m;
    }

    std::shared_ptr<Message> Message::obtain(const std::shared_ptr<Handler> &h, int32_t what)
    {
        auto m = obtain();
        m->target = h;
        m->what = what;
        return m;
    }

    std::shared_ptr<Message> Message::obtain(const std::shared_ptr<Handler> &h, int32_t what, int32_t arg1)
    {
        auto m = obtain();
        m->target = h;
        m->what = what;
        m->arg1 = arg1;
        return m;
    }

    std::shared_ptr<Message> Message::obtain(const std::shared_ptr<Handler> &h, int32_t what, int32_t arg1, int32_t arg2)
    {
        auto m = obtain();
        m->target = h;
        m->what = what;
        m->arg1 = arg1;
        m->arg2 = arg2;
        return m;
    }

    std::shared_ptr<Message> Message::obtain(const std::shared_ptr<Handler> &h, int32_t what, int32_t arg1, int32_t arg2, int32_t arg3)
    {
        auto m = obtain();
        m->target = h;
        m->what = what;
        m->arg1 = arg1;
        m->arg2 = arg2;
        m->arg3 = arg3;
        return m;
    }

    std::shared_ptr<Message> Message::obtain(const std::shared_ptr<Handler> &h, int32_t what, const std::shared_ptr<void> &obj)
    {
        auto m = obtain();
        m->target = h;
        m->what = what;
        m->obj = obj;
        return m;
    }

    std::shared_ptr<Message> Message::obtain(const std::shared_ptr<Handler> &h, int32_t what, int32_t arg1, const std::shared_ptr<void> &obj)
    {
        auto m = obtain();
        m->target = h;
        m->what = what;
        m->arg1 = arg1;
        m->obj = obj;
        return m;
    }

    std::shared_ptr<Message> Message::obtain(const std::shared_ptr<Handler> &h, int32_t what, int32_t arg1, int32_t arg2, const std::shared_ptr<void> &obj)
    {
        auto m = obtain();
        m->target = h;
        m->what = what;
        m->arg1 = arg1;
        m->arg2 = arg2;
        m->obj = obj;
        return m;
    }

    std::shared_ptr<Message> Message::obtain(const std::shared_ptr<Handler> &h, int32_t what, int32_t arg1, int32_t arg2, int32_t arg3, const std::shared_ptr<void> &obj)
    {
        auto m = obtain();
        m->target = h;
        m->what = what;
        m->arg1 = arg1;
        m->arg2 = arg2;
        m->arg3 = arg3;
        m->obj = obj;
        return m;
    }

    void Message::recycle()
    {
        if (isInUse())
        {
            return;
        }
        recycleUnchecked();
    }

    void Message::copyFrom(const std::shared_ptr<Message> &o)
    {
        flags = o->flags & ~kFlagToClearOnCopyFrom;
        what = o->what;
        arg1 = o->arg1;
        arg2 = o->arg2;
        obj = o->obj;
    }

    uint64_t Message::getWhen() const
    {
        return when;
    }

    void Message::setTarget(const std::shared_ptr<Handler> &target)
    {
        this->target = target;
    }

    std::shared_ptr<Handler> Message::getTarget() const
    {
        return target;
    }

    void Message::sendToTarget()
    {
        target->sendMessage(shared_from_this());
    }

    bool Message::isAsynchronous() const
    {
        return (flags & kFlagAsynchronous) != 0;
    }

    void Message::setAsynchronous(bool async)
    {
        if (async)
        {
            flags |= kFlagAsynchronous;
        }
        else
        {
            flags &= ~kFlagAsynchronous;
        }
    }

    std::string Message::toString()
    {
        return toString(SystemClock::uptimeMillis());
    }

    void Message::recycleUnchecked()
    {
        // Mark the message as in use while it remains in the recycled object pool.
        // Clear out all other details.
        flags = kFlagInUse;
        what = 0;
        arg1 = 0;
        arg2 = 0;
        obj = nullptr;
        when = 0ULL;
        target = nullptr;

        {
            std::lock_guard<std::mutex> lock(sPool.mutex_);
            if (sPool.size < sPool.maxSize)
            {
                next = sPool.header;
                sPool.header = shared_from_this();
                sPool.size++;
            }
        }
    }

    bool Message::isInUse() const
    {
        return (flags & kFlagInUse) == kFlagInUse;
    }

    void Message::markInUse()
    {
        flags |= kFlagInUse;
    }

    std::string Message::toString(uint64_t now)
    {
        std::ostringstream ss;

        ss << "{ when=" << when;

        if (target != nullptr)
        {
            ss << " what=" << what;

#ifdef __DEBUG__
            if (!mDebugName.empty())
            {
                ss << "[" << mDebugName << "]";
            }
#endif

            if (arg1 != 0)
            {
                ss << " arg1=" << arg1;
            }

            if (arg2 != 0)
            {
                ss << " arg2=" << arg2;
            }

            if (obj != nullptr)
            {
                ss << " obj=" << obj.get();
            }

#ifdef __DEBUG__
            auto name = target->getDebugName();
            if (!name.empty())
            {
                ss << " target=" << name;
            }
            else
            {
                ss << " target=" << target.get();
            }
#else
            ss << " target=" << target.get();
#endif
        }
        else
        {
            ss << " barrier=" << arg1;
        }

        ss << " }";

        return ss.str();
    }

#ifdef __DEBUG__
    void Message::setDebugName(const std::string &name)
    {
        mDebugName = name;
    }

    std::string Message::getDebugName() const
    {
        return mDebugName;
    }
#endif

} // namespace android
