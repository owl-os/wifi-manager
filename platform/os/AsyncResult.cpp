/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/AsyncResult.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <AsyncResult.hpp>

namespace android
{

    AsyncResult::AsyncResult(const std::shared_ptr<void> &uo, const std::shared_ptr<void> &r, const std::shared_ptr<std::exception> &ex)
        : userObj(uo), result(r), excption(ex)
    {
    }

    std::shared_ptr<AsyncResult> AsyncResult::forMessage(const std::shared_ptr<Message> &m, const std::shared_ptr<void> &r, const std::shared_ptr<std::exception> &ex)
    {
        auto ret = std::make_shared<AsyncResult>(m->obj, r, ex);
        m->obj = ret;
        return ret;
    }

    std::shared_ptr<AsyncResult> AsyncResult::forMessage(const std::shared_ptr<Message> &m)
    {
        auto ret = std::make_shared<AsyncResult>(m->obj, nullptr, nullptr);
        m->obj = ret;
        return ret;
    }

} // namespace telephony
