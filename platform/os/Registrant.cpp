/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/Registrant.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <Registrant.hpp>

namespace android
{

    Registrant::Registrant(const std::shared_ptr<Handler> &h, int32_t w, const std::shared_ptr<void> &obj)
    {
        refH = h;
        what = w;
        userObj = obj;
    }

    Registrant::~Registrant()
    {
    }

    void Registrant::clear()
    {
        refH.reset();
        userObj.reset();
    }

    void Registrant::notifyRegistrant()
    {
        internalNotifyRegistrant(nullptr, nullptr);
    }

    void Registrant::notifyResult(const std::shared_ptr<void> &result)
    {
        internalNotifyRegistrant(result, nullptr);
    }

    void Registrant::notifyException(const std::shared_ptr<std::exception> &ex)
    {
        internalNotifyRegistrant(nullptr, ex);
    }

    void Registrant::notifyRegistrant(const std::shared_ptr<AsyncResult> &ar)
    {
        internalNotifyRegistrant(ar->result, ar->excption);
    }

    void Registrant::internalNotifyRegistrant(const std::shared_ptr<void> &result, const std::shared_ptr<std::exception> &ex)
    {
        auto handler = getHandler();
        if (handler == nullptr)
        {
            clear();
        }
        else
        {
            auto msg = Message::obtain();
            msg->what = what;
            msg->obj = std::make_shared<AsyncResult>(userObj, result, ex);
            handler->sendMessage(msg);
        }
    }

    std::shared_ptr<Message> Registrant::messageForRegistrant()
    {
        auto handler = getHandler();
        if (handler == nullptr)
        {
            clear();
            return nullptr;
        }
        else
        {
            auto msg = handler->obtainMessage();
            msg->what = what;
            msg->obj = userObj;
            return msg;
        }
    }

    std::shared_ptr<Handler> Registrant::getHandler()
    {
        if (refH.expired())
        {
            return nullptr;
        }
        return refH.lock();
    }

} // namespace android
