/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/Looper.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <Looper.hpp>

#include <cassert>

#include <Handler.hpp>
#include <Message.hpp>
#include <MessageQueue.hpp>
#include <Tlog.hpp>

namespace android
{

    namespace
    {

        constexpr const char *const kLogTag = "Looper";

        thread_local std::shared_ptr<Looper> sThreadLocal;
#if defined(PREPARE_MAINLOOPER)
        std::shared_ptr<Looper> sMainLooper;
        std::mutex sMutex;
#endif

    } // namespace

    Looper::Looper(bool quit_allowed) : mQueue(std::make_shared<MessageQueue>(quit_allowed))
    {
        mThreadId = std::this_thread::get_id();
    }

    void Looper::prepare()
    {
        prepare(true);
    }

    void Looper::prepare(bool quit_allowed)
    {
        assert(sThreadLocal == nullptr); // Only one Looper may be created per thread
        sThreadLocal = std::make_shared<Looper>(quit_allowed);
    }

#if defined(PREPARE_MAINLOOPER)
    void Looper::prepareMainLooper()
    {
        prepare(false);
        {
            std::lock_guard<std::mutex> lock(sMutex);
            assert(sMainLooper == nullptr); // The main Looper has already been prepared
            sMainLooper = myLooper();
        }
    }

    std::shared_ptr<Looper> Looper::getMainLooper()
    {
        std::lock_guard<std::mutex> lock(sMutex);
        return sMainLooper;
    }
#endif

    void Looper::loop()
    {
        auto const &me = myLooper();
        assert(me != nullptr); // "No Looper; Looper.prepare() wasn't called on this thread."
        auto const &queue = me->mQueue;

        for (;;)
        {
            auto msg = queue->next(); // might block
            if (!msg)
            {
                return;
            }
            msg->target->dispatchMessage(msg);
            msg->recycleUnchecked();
        }
    }

    std::shared_ptr<Looper> Looper::myLooper()
    {
        return sThreadLocal;
    }

    std::shared_ptr<MessageQueue> Looper::myQueue()
    {
        return myLooper()->mQueue;
    }

    void Looper::quit()
    {
        mQueue->quit(false);
    }

    void Looper::quitSafely()
    {
        mQueue->quit(true);
    }

    std::thread::id Looper::getThreadId() const
    {
        return mThreadId;
    }

    std::shared_ptr<MessageQueue> Looper::getQueue() const
    {
        return mQueue;
    }

} // namespace android
