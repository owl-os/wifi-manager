/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/Message.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANALYZER_MESSAGE_HPP_
#define ANALYZER_MESSAGE_HPP_

#include <memory>
#include <mutex>
#include <string>
#include <vector>
#include <Buffer.hpp>

namespace android
{

    class Handler;
    class Message;

    class Message : public std::enable_shared_from_this<Message>
    {
    public:
        Message();
        #ifndef __UNITTEST__
        virtual ~Message() = default;
        #else
        virtual ~Message();
        #endif

        static std::shared_ptr<Message> obtain();
        static std::shared_ptr<Message> obtain(const std::shared_ptr<Message> &orig);
        static std::shared_ptr<Message> obtain(const std::shared_ptr<Handler> &h);
        static std::shared_ptr<Message> obtain(const std::shared_ptr<Handler> &h, int32_t what);
        static std::shared_ptr<Message> obtain(const std::shared_ptr<Handler> &h, int32_t what, int32_t arg1);
        static std::shared_ptr<Message> obtain(const std::shared_ptr<Handler> &h, int32_t what, int32_t arg1, int32_t arg2);
        static std::shared_ptr<Message> obtain(const std::shared_ptr<Handler> &h, int32_t what, int32_t arg1, int32_t arg2, int32_t arg3);
        static std::shared_ptr<Message> obtain(const std::shared_ptr<Handler> &h, int32_t what, const std::shared_ptr<void> &obj);
        static std::shared_ptr<Message> obtain(const std::shared_ptr<Handler> &h, int32_t what, int32_t arg1, const std::shared_ptr<void> &obj);
        static std::shared_ptr<Message> obtain(const std::shared_ptr<Handler> &h, int32_t what, int32_t arg1, int32_t arg2, const std::shared_ptr<void> &obj);
        static std::shared_ptr<Message> obtain(const std::shared_ptr<Handler> &h, int32_t what, int32_t arg1, int32_t arg2, int32_t arg3, const std::shared_ptr<void> &obj);

        void recycle();
        void copyFrom(const std::shared_ptr<Message> &o);

        uint64_t getWhen() const;

        void setTarget(const std::shared_ptr<Handler> &target);
        std::shared_ptr<Handler> getTarget() const;

        void sendToTarget();

        bool isAsynchronous() const;
        void setAsynchronous(bool async);

        std::string toString();

#ifdef __DEBUG__
        void setDebugName(const std::string &name);
        std::string getDebugName() const;
#endif

    public:
        int32_t what;
        int32_t arg1;
        int32_t arg2;
        int32_t arg3;
        std::shared_ptr<void> obj;
        android::Buffer buffer;

    private:
        void recycleUnchecked();
        bool isInUse() const;
        void markInUse();

        std::string toString(uint64_t now);

    private:
        uint32_t flags;
        uint64_t when;

#ifdef __DEBUG__
        std::string mDebugName;
#endif

        std::shared_ptr<Handler> target;
        std::shared_ptr<Message> next;

        friend class MessageQueue;
        friend class Looper;
        friend class Handler;
    };

} // namespace android

#endif // ANALYZER_MESSAGE_HPP_
