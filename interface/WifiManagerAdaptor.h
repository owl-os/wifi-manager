#ifndef WIFI_MANAGER_ADAPTOR_H_
#define WIFI_MANAGER_ADAPTOR_H_

#include <string>
#include <memory>
#include <glib.h>
#include <map>
#include <set>

#include "GDBusWifiManager.h"
#include "DBusAdaptor.hpp"

class WifiManagerAdaptor : public DBusAdaptor<ComOsOwlWifiManager, WifiManagerAdaptor>
{
public:
    WifiManagerAdaptor();
    virtual ~WifiManagerAdaptor();

    std::string GetServiceName() const override
    {
        return "com.os.owl.WifiManager";
    }

    std::string GetObjectPath() const override
    {
        return "/com/os/owl/WifiManager";
    }

    ComOsOwlWifiManager* AdaptorCreateSkeleton() const override
    {
        return com_os_owl_wifi_manager_skeleton_new();
    }

private:
    WifiManagerAdaptor(const WifiManagerAdaptor&) = delete;
    WifiManagerAdaptor& operator=(const WifiManagerAdaptor&) = delete;
    
};

#endif