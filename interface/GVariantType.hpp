#ifndef GVARIANT_TYPE_H_
#define GVARIANT_TYPE_H_

#include <vector>
#include <glib.h>
#include <Buffer.hpp>

static void GVariantToByteArray(GVariant* in, std::vector<unsigned char>& out)
{
    GVariantIter *iter;
    guchar byte;

    if(in == nullptr)
        return;

    g_variant_get(in, "ay", &iter);
    while(g_variant_iter_loop(iter, "y", &byte)) {
        out.push_back(byte);
    }
    g_variant_iter_free(iter);
}

static void GVariantToByteArray(GVariant* in, std::shared_ptr<android::Buffer>& out)
{
    GVariantIter *iter;
    guchar byte;

    if(in == nullptr)
        return;

    g_variant_get(in, "ay", &iter);
    while(g_variant_iter_loop(iter, "y", &byte)) {
        out->append(byte);
    }
    g_variant_iter_free(iter);
}

static void ByteArrayToGVariant(const std::vector<unsigned char>& in, GVariant** out)
{
    *out = g_variant_new_from_data(G_VARIANT_TYPE("ay"),\
            in.data(), in.size(), true, nullptr, nullptr);
}

static  void ByteArrayToGVariant(const std::shared_ptr<android::Buffer>& in, GVariant** out)
{
    *out = g_variant_new_from_data(G_VARIANT_TYPE("ay"),\
            in->containerPointer()->data(), in->containerPointer()->size(),\
            true, nullptr, nullptr);
}

#endif
