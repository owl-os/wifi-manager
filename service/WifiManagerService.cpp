#include <glib.h>

#include "LogService.h"
#include "WifiManagerService.h"

WifiManagerService::WifiManagerService()
{

}

WifiManagerService::~WifiManagerService()
{

}

bool WifiManagerService::onInit()
{
    LOG_INFO("onInit");

    return true;
}

void WifiManagerService::instantiate()
{
    LOG_INFO("instantiate");
}

error_t WifiManagerService::onStart()
{
    LOG_INFO("onStart WifiManagerService");
    error_t ret = E_OK;

    mWatchDogThread = std::make_shared<android::HandlerThread>("WatchDogThread");
    mWatchDogThread->start();
    mWatchDogHandler = std::make_shared<WatchDogHandler>(mWatchDogThread->getLooper(), *this);
    mWatchDogHandler->sendEmptyMessage(kKickWatchDog);

    mWifiManagerAdaptor = std::make_shared<WifiManagerAdaptor>();
    mWifiManagerAdaptor->RegisterServiceToBus();

    GMainLoop* loop = g_main_loop_new(NULL, false);
    g_main_loop_run(loop);
    g_main_loop_unref(loop);

    mWifiManagerAdaptor->UnregisterServiceFromBus();

    return ret;
}

error_t WifiManagerService::onStop()
{
    return E_OK;
}

WifiManagerService::WatchDogHandler::WatchDogHandler(const std::shared_ptr<android::Looper> &looper, WifiManagerService &service)
    : android::Handler(looper), mService(service)
{
}

WifiManagerService::WatchDogHandler::~WatchDogHandler()
{

}

void WifiManagerService::WatchDogHandler::handleMessage(const std::shared_ptr<android::Message> &msg)
{
    if(msg->what == mService.kKickWatchDog)
    {
        sendEmptyMessageDelayed(mService.kKickWatchDog, mService.kWatchDogInterval);
    }
    else
    {
        LOG_ERROR("Invalid Message [%d]", msg->what);
    }
}