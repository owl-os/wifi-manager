#ifndef WIFI_MANAGER_SERVICE_H
#define WIFI_MANAGER_SERVICE_H

#include <memory>

#include "Handler.hpp"
#include "HandlerThread.hpp"
#include "Looper.hpp"
#include "Message.hpp"
#include "Error.hpp"
#include "WifiManagerAdaptor.h"

class WifiManagerService
{
public:
    int32_t kKickWatchDog = 0;
    int32_t kWatchDogInterval = 20000;
    WifiManagerService();
    virtual ~WifiManagerService();

    virtual bool onInit();
    virtual void instantiate();
    virtual error_t onStart();
    virtual error_t onStop();

    class WatchDogHandler final : public android::Handler
    {
    public:
        WatchDogHandler(const std::shared_ptr<android::Looper> &looper, WifiManagerService &service);
        virtual ~WatchDogHandler();

        void handleMessage(const std::shared_ptr<android::Message> &msg) override;
    private:
        WifiManagerService& mService;
    };

private:
    std::shared_ptr<android::HandlerThread> mWatchDogThread;
    std::shared_ptr<WifiManagerAdaptor> mWifiManagerAdaptor;
    std::shared_ptr<android::Handler> mWatchDogHandler;
};

#endif